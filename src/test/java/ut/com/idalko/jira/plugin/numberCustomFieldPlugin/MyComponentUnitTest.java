package ut.com.idalko.jira.plugin.numberCustomFieldPlugin;

import org.junit.Test;
import com.idalko.jira.plugin.numberCustomFieldPlugin.api.MyPluginComponent;
import com.idalko.jira.plugin.numberCustomFieldPlugin.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}